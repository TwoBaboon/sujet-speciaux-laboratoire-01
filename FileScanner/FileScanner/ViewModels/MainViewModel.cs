﻿using FileScanner.Commands;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using System.Drawing;
using System.Diagnostics;

namespace FileScanner.ViewModels
{
    public class MainViewModel : BaseViewModel
    {



        private string selectedFolder;
        private ObservableCollection<string> folderItems = new ObservableCollection<string>();

        //---------------------------------------
        private ObservableCollection<Type> types = new ObservableCollection<Type>();
        public ObservableCollection<Type> Types
        {
            get => types;
            set
            {
                types = value;
                OnPropertyChanged();

            }
        }
        //---------------------------------------




        public DelegateCommand<string> OpenFolderCommand { get; private set; }
        public DelegateCommand<string> ScanFolderCommand { get; private set; }

        public ObservableCollection<string> FolderItems { 
            get => folderItems;
            set 
            { 
                folderItems = value;
                OnPropertyChanged();

             }
        }


        public string SelectedFolder
        {
            get => selectedFolder;
            set
            {
                selectedFolder = value;
                OnPropertyChanged();
                ScanFolderCommand.RaiseCanExecuteChanged();
            }
        }

        public MainViewModel()
        {
            OpenFolderCommand = new DelegateCommand<string>(OpenFolder);
            ScanFolderCommand = new DelegateCommand<string>(ScanFolder, CanExecuteScanFolder);
        }

        private bool CanExecuteScanFolder(string obj)
        {
            return !string.IsNullOrEmpty(SelectedFolder);
        }

        private void OpenFolder(string obj)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                if (fbd.ShowDialog() == DialogResult.OK)
                {
                    SelectedFolder = fbd.SelectedPath;
                }
            }
        }

        private void ScanFolder(string dir)
        {
            FolderItems = new ObservableCollection<string>(GetDirFiles(dir));

            
            foreach (var item in Directory.EnumerateFiles(dir, "*"))
            {
                var c = new Type { Chemin = item, Photo = "/Images/note.png" };
                FolderItems.Add(item);
                Debug.WriteLine(item);

                if (File.Exists(item))
                {
                    types.Add(c);
                    //System.Windows.MessageBox.Show("[fichier]" + item);
                }


            }
        }

        IEnumerable<string> GetDirFiles(string dir)
        {
            foreach (var item in Directory.EnumerateDirectories(dir, "*"))
            {
                var c = new Type { Chemin = item, Photo = "/Images/folder.png" };
                if (Directory.Exists(item))
                {
                    types.Add(c);
                    //System.Windows.MessageBox.Show("[dossier]" + item);
                }
                yield return item;
            }
        }


    }
}
